# PCM v1.0
A parsimonious canopy model to predict GPP and LAI for deciduous broad-leaved forests.
All source code, configuration and parameter files are subject to copyright (C) please see the file COPYRIGHT.


# Description
This repository contains R codes for the open-source distribution of PCM. The model is planned to be submitted to Geoscientific Model Development journal.
The PCM simulates processes related to carbon cycle in the canopy at forest stand of undetermined size. When soil moisture data is available, the PCM allows to consider the impact of water stress on the GPP and LAI. 

The model has been developed on macOS Big Sur version 11.2.1 using R 4.0.3 . It is recommended to use R version >= 4.0.

# To run main.R you will need following packages:
•   zoo
•   Metrics
•   hydroGOF
•   tidyverse
•   lubridate
•   imputeTS
•   bigleaf
•   forecast
! In case of error when installing package "forecast" on macOS, user may answer "no" to this question, "Do you want to install from sources the package which needs compilation? (Yes/no/cancel)"


# Data to run the model: 
- T (T_air), VPD, PPFD, GPP, preferably in one csv file. 
- SM (soil moisture) as a csv file.
- A csv file containing December temperature of the year before of the starting year of simulation.
- If available a csv file of field-measurement LAI (only for plotting at the end and making comparison with modelled LAI).
- Separate csv files for each soil profile (including sand, clay contents \%, bulk density, and depth information). 
- A ".dat" file which includes upper, lower boundaries, or a set of recommended parameters to run the model.

# User should also identify some values in ConfigFile:
 - Environment setting path.
 - Identify files to be read as infile.
 - Site latitude in degree decimal.
 - Number of soil profiles: e.g., 2. 
 - LAIini: e.g., 0.35.
 - betta_fcpwp which is root distribution coefficient for DBF biome, e.g., 0.966.
 - Model needs to be informed about duration of simulation; stday (related to the index of starting day of simulation in the climate data file) and enday (related to the index of end day in the climate data file).
 - Call required libraries.  

# License
Please see the LICENSE.md file for license information. 


