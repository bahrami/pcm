# Function to calculate day length in hour
# B. Bahrami

lat_rad<-lat_deg*(pi/180)
daylength <- function(lat_deg,pi, jday) {
  delta_t <- -23.4*(pi/180)*cos(2*pi*(jday+10)/365)
  hhalf_t <- acos(-(sin(lat_rad)*sin(delta_t))/(cos(lat_rad)*cos(delta_t)))
  daylength_t <- 24 *(hhalf_t/pi) 
  return(daylength_t)
}
