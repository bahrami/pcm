# This is a sub_model of the PCM Model for calculating the FC and PWP.
# Bahar Bahrami, CHS, UFZ, Germany. Last modification: August 2020

# Creating a pedotransfer function named theta.fun
theta.fun<-function(num_profiles,psi,depth,clay,sand,bd,beta_fcpwp) {
  clay <- clay  
  sand <- sand  
  depth <- depth
  bd <- bd
  beta_fcpwp<-beta_fcpwp
  num_profiles <- num_profiles
  psi <-psi
  ################### to create weights for fc and pwp:
  depth1 <- c(0,depth)
  num_layers <- length(depth1)
  rf_cum<- matrix(NA,num_layers)
  rf_indi1 <- matrix(NA,num_layers)
  rf_cum[1] <- 0
  rf_indi1[1] <- 0
  
  for (i in 2:num_layers){
    rf_cum[i] <- 1-(beta_fcpwp**depth1[i])
  }
   normalize <-  tail(rf_cum[i], n=1)
   rf_cum <- rf_cum/normalize
    
   for (i in 2:num_layers){
     rf_indi1[i] <- rf_cum[i]-rf_cum[i-1]
   }
   rf_indi1 <- rf_indi1[-1]
  ###################
 # theta_fcpwp<- matrix(NA, length(depth),2)
  theta_fcpwp <- array(NA, c(num_profiles, length(depth),2));
 for (k in 1:num_profiles) {
  #cat("Profile num=", k,"\n")
  theta<- matrix(NA, length(depth),length(psi))
  for (i in 1:length(depth)) {
    #for (i in 1:4) {
    for (j in 1:15000) {
      ths <- 0.788 + 0.001 *clay[i,k] -0.263* bd[i,k]
      thr <- 0
      alpha <- exp(-0.648+(0.023*sand[i,k])+(0.04*clay[i,k])-(3.168*bd[i,k]))
      n <- 1.392-(0.418*(sand[i,k]**-0.024)) + 1.212*(clay[i,k]**-0.704)
      m <- 1-(1/n)
      z1<-(1+(abs(alpha*psi[j]))^n)^m
      z2 <- ths - thr
      theta[i,j]<-thr+(z2/z1)
    } #end loop over psi
    theta_fcpwp [k,i,1]<- theta[i,330]
    #cat("theta_fc=", theta_fcpwp [k,i,1],"\n")
    #cat("weight=", rf_indi1[i],"\n")
    theta_fcpwp [k,i,1]<- theta[i,330]*rf_indi1[i] # 330 hpa, 33 kpa; fc can be ranged between 100-330 hPa (Singh, 2007)
    #cat("weighted theta_fc=", theta_fcpwp [k,i,1],"\n")
    theta_fcpwp [k,i,2]<- theta[i,15000] #15000hpa, 1500 kpa
    #cat("theta_pwp=", theta_fcpwp [k,i,2],"\n")
    theta_fcpwp [k,i,2]<- theta[i,15000]*rf_indi1[i]
    #cat("theta_pwp=", theta_fcpwp [k,i,2],"\n")
    #cat("-------------------------","\n")
 } #end loop depths
  
  
} #end loop over k profiles
  fcsum <- matrix(NA,num_profiles)
  pwpsum <- matrix(NA,num_profiles)
  for (i in 1:num_profiles) {
  fcsum[i] <- sum(theta_fcpwp[i, ,1])
  pwpsum[i] <- sum(theta_fcpwp[i, ,2])
  }
  fc <- mean(fcsum)
  #fc <- fcsum
  pwp <- mean(pwpsum)
  #pwp <- pwpsum
  return(list(fc=fc,pwp=pwp))
}